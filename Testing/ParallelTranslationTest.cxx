#include "GeodesicShooting.h"
#include <time.h>

struct params
{
  int numStep;
  int lpower;
  int truncX;
  int truncY;
  int truncZ;
  float sigma;
  float alpha;
  float gamma;
  int maxIter;
  float stepSizeGD;
  int memType;
  MemoryType mType;
  bool doRK4;
};

void doMatching(Image3D* I0, Image3D* I1, char* outImageFile, char* outPhiFile, char* outFieldFile,
  const params& parms)
{
  // access parameter
  GridInfo grid = I0->grid();
  Vec3Di mSize = grid.size();

  int fsx = mSize.x;
  int fsy = mSize.y;
  int fsz = mSize.z;

  FftOper *fftOper = new FftOper(parms.alpha, parms.gamma, parms.lpower, grid,
                                 parms.truncX, parms.truncY, parms.truncZ);
  fftOper->FourierCoefficient();

  // forward shooting
  FieldComplex3D *v0 = new FieldComplex3D(parms.truncX, parms.truncY, parms.truncZ);
  Field3D *v0Spatial = new Field3D(grid, parms.mType);
  FieldComplex3D *gradv = new FieldComplex3D(parms.truncX, parms.truncY, parms.truncZ);

  GeodesicShooting *geodesicshooting = new GeodesicShooting(fftOper, parms.mType, parms.numStep);

  
  Opers::Copy(*(geodesicshooting->I0), *I0);
  Opers::Copy(*(geodesicshooting->I1), *I1);
  geodesicshooting->sigma = parms.sigma;
  
  clock_t t;
  t = clock();

  geodesicshooting->ImageMatching(*v0, *gradv, parms.maxIter, parms.stepSizeGD);

  t = clock() - t;
  printf("It took me %lu clicks (%f seconds).\n",t,((float)t)/CLOCKS_PER_SEC);
  printf("Final energy: %f .\n",geodesicshooting->IEnergy);
 
  ITKFileIO::SaveImage(*(geodesicshooting->deformIm), outImageFile);
  Field3D *phiinv = new Field3D(grid, parms.mType);
  Opers::HtoV(*phiinv, *(geodesicshooting->phiinv));
  //ITKFileIO::SaveField(*(geodesicshooting->phiinv), outPhiFile);
  ITKFileIO::SaveField(*phiinv, outPhiFile);

  fftOper->fourier2spatial(*v0Spatial, *v0);
  ITKFileIO::SaveField(*v0Spatial, outFieldFile);

  delete fftOper;
  delete geodesicshooting;
  delete v0;
  delete v0Spatial;
  delete gradv;
  delete phiinv;
}

int main(int argc, char** argv)
{
  char I00Path[100];
  char I01Path[100];
  char I10Path[100];
  char I11Path[100];

  int argi = 1; 
  strcpy(I00Path, argv[argi++]);
  strcpy(I01Path, argv[argi++]);
  strcpy(I10Path, argv[argi++]);
  strcpy(I11Path, argv[argi++]);
  params parms;
  parms.numStep = atoi(argv[argi++]);
  parms.lpower = atoi(argv[argi++]);
  parms.truncX = atoi(argv[argi++]);
  parms.truncY = atoi(argv[argi++]);
  parms.truncZ = atoi(argv[argi++]);
  parms.sigma = atof(argv[argi++]);
  parms.alpha = atof(argv[argi++]);
  parms.gamma = atof(argv[argi++]);
  parms.maxIter = atoi(argv[argi++]);
  parms.stepSizeGD = atof(argv[argi++]);
  parms.memType = atoi(argv[argi++]);
  parms.doRK4 = atoi(argv[argi++]) > 0;

  // precalculate low frequency location
  if (parms.truncX % 2 == 0) parms.truncX -= 1; // set last dimension as zero if it is even
  if (parms.truncY % 2 == 0) parms.truncY -= 1;
  if (parms.truncZ % 2 == 0) parms.truncZ -= 1;

  // runs on CPU or GPU
  if (parms.memType == 0)
    parms.mType = MEM_HOST;
  else 
    parms.mType = MEM_DEVICE;
	  
  // read data
  Image3D *I0, *I1, *I00, *I01, *I10, *I11;
  I00 = new Image3D(parms.mType);
  I01 = new Image3D(parms.mType);
  I10 = new Image3D(parms.mType);
  I11 = new Image3D(parms.mType);

  ITKFileIO::LoadImage(*I00, I00Path);
  ITKFileIO::LoadImage(*I01, I01Path);
  ITKFileIO::LoadImage(*I10, I10Path);
  ITKFileIO::LoadImage(*I11, I11Path);

  doMatching(I00, I01, "deformIm00_01.mhd", "phiInv00_01.mhd", "v0Spatial00_01.mhd", parms);
  doMatching(I10, I11, "deformIm10_11.mhd", "phiInv10_11.mhd", "v0Spatial10_11.mhd", parms);
  doMatching(I00, I10, "deformIm00_10.mhd", "phiInv00_10.mhd", "v0Spatial00_10.mhd", parms);

  // now transport the I00-I01 v0 to the I00-I10 atlas space.
  Field3D *v0Spatial = new Field3D(parms.mType);
  Field3D *w0Spatial = new Field3D(parms.mType);
  ITKFileIO::LoadField(*v0Spatial, "v0Spatial00_10.mhd");
  ITKFileIO::LoadField(*w0Spatial, "v0Spatial00_01.mhd");
  GridInfo grid = v0Spatial->grid();
  Vec3Di mSize = grid.size();

  int fsx = mSize.x;
  int fsy = mSize.y;
  int fsz = mSize.z;

  FftOper *fftOper = new FftOper(parms.alpha, parms.gamma, parms.lpower, grid,
                                 parms.truncX, parms.truncY, parms.truncZ);
  fftOper->FourierCoefficient();
  FieldComplex3D *v0 = new FieldComplex3D(parms.truncX, parms.truncY, parms.truncZ);
  FieldComplex3D *w0 = new FieldComplex3D(parms.truncX, parms.truncY, parms.truncZ);
  FieldComplex3D *parTransvw = new FieldComplex3D(parms.truncX, parms.truncY, parms.truncZ);
  Field3D *parTransvwSpatial = new Field3D(grid, parms.mType);

  fftOper->spatial2fourier(*v0, *v0Spatial);
  fftOper->spatial2fourier(*w0, *w0Spatial);

  GeodesicShooting *geodesicshooting = new GeodesicShooting(fftOper, parms.mType, parms.numStep, parms.doRK4);

  clock_t t;
  t = clock();

  geodesicshooting->parallelTranslate(*parTransvw, *v0, *w0);

  t = clock() - t;
  printf("It took me %lu clicks (%f seconds).\n",t,((float)t)/CLOCKS_PER_SEC);

  fftOper->fourier2spatial(*parTransvwSpatial, *parTransvw);
  ITKFileIO::SaveField(*parTransvwSpatial, "parTransvwSpatial.mhd");

  FieldComplex3D *tmpgrad = new FieldComplex3D(parms.truncX, parms.truncY, parms.truncZ);

  Opers::Copy(*(geodesicshooting->I0), *I10);
  geodesicshooting->fwdIntegration(*parTransvw, *tmpgrad);
  ITKFileIO::SaveImage(*(geodesicshooting->deformIm), "deformIm.mhd");

  Field3D *phiinv = new Field3D(grid, parms.mType);
  Opers::HtoV(*phiinv, *(geodesicshooting->phiinv));
  //ITKFileIO::SaveField(*(geodesicshooting->phiinv), "phiInvParTransvw.mhd");
  ITKFileIO::SaveField(*phiinv, "phiInvParTransvw.mhd");

  delete fftOper;
  delete geodesicshooting;
  delete I00;
  delete I01;
  delete I10;
  delete I11;
  delete v0;
  delete w0;
  delete parTransvw;
  delete v0Spatial;
  delete w0Spatial;
  delete parTransvwSpatial;
  delete tmpgrad;
  delete phiinv;

}
