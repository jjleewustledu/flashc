#!/bin/bash
execPath="BINARY_PATH"
dataPath="DATA_PATH"
subj1T1Path="${dataPath}/Image_0.mhd"
subj1T2Path="${dataPath}/Image_1.mhd"
subj2T1Path="${dataPath}/Image_7.mhd"
subj2T2Path="${dataPath}/Image_3.mhd"
numStep=100
lpower=3
truncX=16
truncY=16
truncZ=16 # if 2D, pass z as 1 # 16 for 3D
sigma=0.03
alpha=3.0 # 3.0
gamma=1.0
maxIter=200
stepSizeGD=5.0e-2
mType=0 # 0: Host; 1: DEVICE
doRK4=1 # 0: Euler; 1: Runge-Kutta 4 integration
 
${execPath}/ParallelTranslationTest ${subj1T1Path} ${subj1T2Path} ${subj2T1Path} ${subj2T2Path} ${numStep} ${lpower} ${truncX} ${truncY} ${truncZ} ${sigma} ${alpha} ${gamma} ${maxIter} ${stepSizeGD} ${mType} ${doRK4}
